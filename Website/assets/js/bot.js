var accessToken = "01f31b4897544f9cb76d5748ea4adabb";
var userentry;
var setLoadingResponse;
var html;
var counter = 1;
var box1 = document.getElementById("chat-history");
var baseUrl = "https://api.api.ai/v1/";
$(document).ready(function() {
    $("#input").keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            send();
            submit();


        }
    });
    $("#rec").click(function(event) {
        switchRecognition();
    });
});
var recognition;
function startRecognition() {
    recognition = new webkitSpeechRecognition();
    recognition.onstart = function(event) {
        updateRec();
    };
    recognition.onresult = function(event) {
        var text = "";
        for (var i = event.resultIndex; i < event.results.length; ++i) {
            text += event.results[i][0].transcript;
        }
        setInput(text);
        stopRecognition();
    };
    recognition.onend = function() {
        stopRecognition();
    };
    recognition.lang = "en-US";
    recognition.start();
}

function stopRecognition() {
    if (recognition) {
        recognition.stop();
        recognition = null;
    }
    updateRec();
}
function submit() {
    html = "<div class='chat-message clearfix'> \
    <img src='images/profile.png' alt='' width='32' height='32'> \
    <div class='chat-message-content clearfix'> \
    <span class='chat-time'>13:35</span> \
    <h5>User: </h5> \
    <p id='userentry2'></p> \
    </div> <!-- end chat-message-content --> \
    </div> <!-- end chat-message --> \
    <hr>";


    box1.innerHTML += html;
    document.getElementById("userentry2").innerHTML += userentry;
    $('#userentry2').removeAttr('id');
}
function switchRecognition() {
    if (recognition) {
        stopRecognition();
    } else {
        startRecognition();
    }
}
function setInput(text) {
    $("#input").val(text);
    send();
}
function updateRec() {
    $("#rec").text(recognition ? "Stop" : "Speak");
}
function send() {
    var text = $("#input").val();
    userentry = text;
    console.log(userentry);
    $.ajax({
        type: "POST",
        url: baseUrl + "query?v=20150910",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        data: JSON.stringify({ query: text, lang: "en", sessionId: "somerandomthing" }),
        success: function(data) {
            var respText = data.result.fulfillment.speech;
            console.log("Respuesta: " + respText);
            setResponse(respText);
            document.getElementById("feedback").innerHTML = "  ";
            html2 = "<div class='chat-message clearfix' id='bottom'> \
            <img src='images/avatar.png' alt='' width='32' height='32'> \
            <div class='chat-message-content clearfix'> \
            <span class='chat-time'>13:35</span> \
            <h5>Alfred: </h5> \
            <p id='userentry3'></p> \
            </div> <!-- end chat-message-content --> \
            </div> <!-- end chat-message --> \
            <hr>";

            counter++;
            console.log(counter);
            document.getElementById("counter").innerHTML = counter;


            box1.innerHTML += html2;
            document.getElementById("userentry3").innerHTML += respText;
            $('#userentry3').removeAttr('id');

            document.getElementById("input").value = "";

            document.getElementById( 'bottom' ).scrollIntoView();

            $('#bottom').removeAttr('id');
        },
        error: function() {
            setResponse("Internal Server Error");
        }
    });
setLoadingResponse = "Thinking...";
document.getElementById("feedback").innerHTML = setLoadingResponse;
}
function setResponse(val) {
    $("#response").text(val);
}
(function() {

    $('#live-chat header').on('click', function() {

        $('.chat').slideToggle(300, 'swing');
        $('.chat-message-counter').fadeToggle(300, 'swing');

    });

    $('.chat-close').on('click', function(e) {

        e.preventDefault();
        $('#live-chat').fadeOut(300);

    });

}) ();