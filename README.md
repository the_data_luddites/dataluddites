[Matt]: images/team/Matt.JPG
[Scott]: images/team/Scott.JPG
[Zac]: images/team/Zac.JPG
[Gavin]: images/team/Gavin.JPG
[Kristin]: images/team/Kristin.JPG
[Elisha]: images/team/Elisha.JPG
[Jackie]: images/team/Jackie.JPG

#The Data Luddites
---

## Age Care Choices


### 1.1 - What/Who are we

Discover Age Care Centres in your local area which align with your interests, values and health care needs. 
Age Care Choices is made just for you. Answer our personalised questionnaire and you will be matched with the residential centres who can cater to your interests, health needs and favoured facilities within your area of choice. 
Once you are living in your new centre, you and or your loved ones can rate your experience, and in turn empower others make the right choice about their Age Care future.


###1.2 - The Team



* Matthew - Developer, Data analyst ![Image of Matt][Matt]
* Scott - Creative ![Image of Scott][Scott]
* Kristin - Film, Creative ![Image of Kristin][Kristin]
* Zac - Developer ![Image of Zac][Zac]
* Gavin - Mentor ![Image of Gavin][Gavin]

###1.3 - The Product

######Website Platform Beta:
http://agecarechoices.space/


###1.4 - Special Mentions

* Elisha Dibbens ![Image of Elisha][Elisha]
* Jackie Harris ![Image of Jackie][Jackie]